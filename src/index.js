import Tchat from './tchat';

import './index.scss';

const bots = [{
  id: '1',
  name: 'Luffy',
  avatar: 'https://pbs.twimg.com/profile_images/1236296297134596100/THisrIV2_400x400.jpg" class="img-fluid rounded-circle border border-secondary border-2',
  countMessage: 0,
  actions: [{
      name: 'hello',
      keywords: ['hello', 'bonjour'],
      action: () => 'bonjour Mathieu'
  }]
}, {
  id: '2',
  name: 'Naruto',
  avatar: 'https://chocobonplan.com/wp-content/uploads/2020/03/naruto-shippuden-gratuit.jpg" class="img-fluid rounded-circle border border-secondary border-2',
  countMessage: 0,
  actions: [{
      name: 'hello',
      keywords: ['hello', 'bonjour'],
      action: () => 'bonjour Mathieu'
  }]
}, {
  id: '3',
  name: 'Izuku',
  avatar: 'https://qph.fs.quoracdn.net/main-qimg-3a393316ba586c7f08169dea02330ef5" class="img-fluid rounded-circle border border-secondary border-2',
  countMessage: 0,
  actions: [{
      name: 'hello',
      keywords: ['hello', 'bonjour'],
      action: () => 'bonjour Mathieu'
  }]
}
];

const tchat = new Tchat(bots);

tchat.run();
