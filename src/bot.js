import { convertTypeAcquisitionFromJson } from "typescript";

const Bot = class {
    constructor(entity) {
        this.entity = entity;
    }

    findActionByValue(value) {
        const { action } = this.entity;
        
        for(let i = 0; i < actions.length; i++) {
            const action = actions[i];

            for (let j = 0; j < action.keyword.length; j++) {
                const keyword = action.keyword[j];
                
                if (value === keyword) {
                    return action.action();
                }
            }
        }

        return false;
    }
};

export default Bot;